# Playgap

## Integration Steps

1) **"Install"** or **"Upload"** FGPlaygap plugin from the FunGames Integration Manager in Unity, or download it from here.

2) Click on the **"Prefabs and Settings"** button in the FunGames Integration Manager to fill up your scene with required components and create the Settings asset.
